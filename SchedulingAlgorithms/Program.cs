﻿using SchedulingAlgorithms.Model;
using SchedulingAlgorithms.Algorithms;
using System;
using System.Globalization;
using System.IO;
#nullable enable

namespace SchedulingAlgorithms {
    class Program {
        private const string lpRelaxationFileExt = "_relaxation.lp";
        private readonly RelaxedLP model;
        public Program(ImportConfig config) {
            model = new RelaxedLP(config);
        }
        static void Main(string[] args) {
            var interactive = false;
            var file = string.Empty;
            if (args.Length < 1) {
                Console.WriteLine("No argument specified: defaulting to interactive mode!");
            } else {
                file = args[0];
            }

            if (file == "-i" || file == string.Empty)
                interactive = true;

            if (LoadConfig(interactive, file, out var config))
                try {
                    new Program(config).Run();
                } catch (Exception e) {
                    Console.WriteLine("Unhandled exception: {0}", e.Message);
                    throw;
                }

            Console.WriteLine("Press ENTER to exit...");
            Console.ReadLine();
        }

        private static bool LoadConfig(bool interactive, string file, out ImportConfig config) {
            config = new ImportConfig();
            if (interactive) {
                Console.WriteLine("Interactive mode! Enter your model and enter \"break\" to finish:");
                config = ModelParser.ParseInteractive();
            } else {
                var filename = Path.GetFileName(file);
                Console.WriteLine("File mode! Reading model from file '{0}'!", filename);
                try {
                    config = ModelParser.Parse(file);
                } catch (FormatException e) {
                    Console.WriteLine("Parsing error: {0}", e.Message);
                    return false;
                } catch (FileNotFoundException e) {
                    Console.WriteLine("File '{1}' not found: {0}", e.Message, e.FileName);
                    return false;
                } catch (DirectoryNotFoundException e) {
                    Console.WriteLine("Directory not found: {0}", e.Message);
                    return false;
                } catch (IOException e) {
                    Console.WriteLine("I/O exception: {0}", e.Message);
                    return false;
                }
            }
            return true;
        }

        private void Run() {
            var lpProgramFile = CreateLpProgram(model);
            Schedule(lpProgramFile, model);
        }

        private void Schedule(string file, RelaxedLP model) {
            // calculate start times in conf.jobs
            var solver = new LpSolve(file);
            Console.WriteLine("Starting lp_solve...");
            IOutputParser result;
            try {
                result = solver.Solve(out var processingTime);
                Console.WriteLine("Successfully solved relaxed problem in {0}ms!", processingTime.TotalMilliseconds);
            } catch (OperationCanceledException e) {
                Console.WriteLine("Solving cancelled: {0}", e.Message);
                return;
            } catch (InvalidOperationException e) {
                Console.WriteLine("Solving failed: {0}", e.Message);
                return;
            }
            model.ParseResults(result);
            var solutionFile = WriteSolutionToCsv(model);
            Console.WriteLine("Solution written to '{0}'...", solutionFile);
            var fi = new FileInfo(solutionFile);
            Console.WriteLine(fi.FullName);
            var scheduler = new LpAlphaScheduler(model);
            var schedule = scheduler.GenerateSchedule();
            for (var i = 0; i < schedule.JobCount; i++) {
                var scheduledJob = schedule[i];
                Console.WriteLine("Job '{0}' is scheduled in time [{1},{2}) on machine {3}!", model.Jobs[i].Name, scheduledJob.StartTime, scheduledJob.FinishTime, scheduledJob.MachineIndex);
            }
            //PlotMachineUsage(model, objectiveTime);
        }

        private static string WriteSolutionToCsv(RelaxedLP model) {
            if (model.Solution is null)
                throw new InvalidOperationException("Model is not solved!");

            var filename = model.Name + "_relaxed.csv";
            using var sw = new StreamWriter(filename);
            sw.Write("Time:");
            for (var i = 1; i <= model.UpperTimeLimit; i++)
                sw.Write(",{0}", i);
            sw.WriteLine();
            for (var j = 0; j < model.Jobs.Length; j++) {
                sw.Write(model.Jobs[j].Name);
                for (var t = 0; t < model.UpperTimeLimit; t++)
                    sw.Write(",{0}", model.Solution[j, t].ToString(CultureInfo.InvariantCulture));
                sw.WriteLine();
            }
            sw.Flush();
            return filename;
        }

        private string CreateLpProgram(RelaxedLP cnf) {
            var file = cnf.Name + lpRelaxationFileExt;
            using (var sw = new StreamWriter(file))
                cnf.WriteLpProgram(sw);
            Console.WriteLine("Written to {0}!", file);
            return file;
        }
    }
}
