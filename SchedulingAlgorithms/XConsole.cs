﻿using SchedulingAlgorithms.Model;
using System;
using System.Collections.Generic;
#nullable enable

namespace SchedulingAlgorithms {
    public static class XConsole {
        public static Dictionary<string, Job> ReadJoblist(int N) {
            var jobs = new Dictionary<string, Job>();
            for (var i = 0; i < N; i++) {
                Console.Write("Enter job {0} name: ", i);
                var jobname = Console.ReadLine();
                Console.WriteLine("Specify job {0}: ", jobname);
                jobs.Add(jobname, ReadJob());
                Console.WriteLine();
            }

            return jobs;
        }
        public static int ReadInt() {
            while (true) {
                if (int.TryParse(Console.ReadLine(), out var result))
                    return result;
                Console.Write("Invalid integer! Try again: ");
            }
        }
        public static bool ReadBool() {
            while (true) {
                switch (Console.ReadKey(true).Key) {
                case ConsoleKey.J:
                case ConsoleKey.Y:
                    return true;
                case ConsoleKey.N:
                    return false;
                default:
                    break;
                }
                Console.Write("Invalid boolean! Try again (y/n): ");
            }
        }
        public static double ReadDouble() {
            while (true) {
                if (double.TryParse(Console.ReadLine(), out var result))
                    return result;
                Console.Write("Invalid double! Try again: ");
            }
        }

        public static Job ReadJob() {
            Console.Write("Job weight: ");
            var weight = ReadDouble();
            Console.Write("Job time: ");
            var time = ReadInt();
            return new Job(time, weight);
        }
    }
}
