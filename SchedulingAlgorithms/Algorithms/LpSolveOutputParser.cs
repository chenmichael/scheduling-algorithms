﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
#nullable enable

namespace SchedulingAlgorithms.Algorithms {
    public class LpSolveOutputParser : IOutputParser {
        private readonly string ProgramOutput;
        private bool success = false;
        private double objectiveValue;
        private readonly Dictionary<string, double> resultValues = new Dictionary<string, double>();
        public LpSolveOutputParser(string output) {
            ProgramOutput = output.Replace("\r\n", "\n");
            Parse();
        }
        private void AssertProgramFeasible() {
            if (!success)
                throw new InvalidOperationException("Program is infeasible!");
        }
        public double GetDoubleValue(string variable) {
            AssertProgramFeasible();
            return resultValues[variable];
        }
        public int GetIntValue(string variable) {
            AssertProgramFeasible();
            return AsIntUnrounded(resultValues[variable]);
        }
        private int AsIntUnrounded(double v) {
            if (Math.Floor(v) != v)
                throw new ArgumentOutOfRangeException($"Variable '{v}' has no integral value!");
            return Convert.ToInt32(v);
        }
        public double ObjectiveValue() {
            AssertProgramFeasible();
            return objectiveValue;
        }
        public bool SolveSuccess() => success;
        private void Parse() {
            var infeasibleMatch = new Regex(@"This problem is infeasible").Match(ProgramOutput);

            if (infeasibleMatch.Success)
                return;
            success = true;

            var objectiveMatch = new Regex(@"Value of objective function: (\d+(?:\.\d+)?)").Match(ProgramOutput);

            if (!objectiveMatch.Success)
                throw new ArgumentException("Could not retreive objective value!");
            objectiveValue = double.Parse(objectiveMatch.Groups[1].Value, CultureInfo.InvariantCulture);
            Console.WriteLine("Solution found objective time {0}!", objectiveValue);

            var variableRgx = new Regex(@"^(?<name>[a-zA-z][\w\[\]\{\}\/\.\&\#\$\%\~\'\@\^]*)\s+(?<value>\d+(?:\.\d+)?)$", RegexOptions.Multiline);
            var matches = variableRgx.Matches(ProgramOutput);
            for (var i = 0; i < matches.Count; i++) {
                var match = matches[i];

                var varName = match.Groups["name"].Value;
                var varValue = double.Parse(match.Groups["value"].Value, CultureInfo.InvariantCulture);
                resultValues.Add(varName, varValue);
            }
        }
    }
}