﻿using System;

namespace SchedulingAlgorithms.Algorithms {
    public interface ISolver {
        string GetSolverExecutable();
        IOutputParser Solve(out TimeSpan processingTime);
    }
}