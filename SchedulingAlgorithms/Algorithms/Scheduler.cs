﻿using SchedulingAlgorithms.Model;
using System;

namespace SchedulingAlgorithms.Algorithms {
    public class Scheduler {
        protected readonly LpModel model;
        public Scheduler(LpModel model) {
            if (!model.IsSolved)
                throw new InvalidOperationException("Cannot schedule on an unsolved model!");
            this.model = model;
        }
    }
}