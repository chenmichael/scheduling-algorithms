﻿using SchedulingAlgorithms.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchedulingAlgorithms.Algorithms {
    public class LpAlphaScheduler : Scheduler {
        private readonly Random rnd;
        public LpAlphaScheduler(LpModel model) : base(model) {
            rnd = new Random();
        }
        public Schedule GenerateSchedule() {
            if (model.Solution is null)
                throw new ArgumentNullException("Solution is null!");

            var x = model.Solution;
            var jobcount = x.GetLength(0);
            if (jobcount != model.Jobs.Length)
                throw new ArgumentOutOfRangeException("Wrong dimensions for x matrix!");
            var maxTime = x.GetLength(1);

            var schedule = new Schedule(model.Jobs);
            var c = CalculateRelaxedJobCompletion(x, maxTime);

            var L = new double[model.Machines];
            for (var i = 0; i < model.Machines; i++)
                L[i] = 1.0d;
            // NextDouble() \in [0,1)
            // 1.0d - NextDouble() \in (0,1]
            // alpha in (0,1/2]
            var alpha = (1.0d - rnd.NextDouble()) * 0.5d;
            var cLpAlpha = CalculateAlphaJobCompletion(jobcount, c, alpha);

            while (cLpAlpha.Count > 0) {
                // Get min cAlphaJ item tiebraking by first come first serve
                var (k, cAlpha) = cLpAlpha.Aggregate((i1, i2) => (i1.cAlpha < i2.cAlpha) ? i1 : i2);
                cLpAlpha.RemoveAll(i => i.j == k);

                var maxcjdjk = 0.0d;
                foreach (var prec in model.Precedences)
                    if (prec.K == k)
                        maxcjdjk = Math.Max(maxcjdjk, c[prec.J] + prec.Delay);

                // Set S_k (automatically sets C_k) => start and finish
                var nextFreeMachine = L.Min();
                var earliestStart = Math.Max(maxcjdjk, nextFreeMachine);
                schedule[k].ScheduleFromStart(earliestStart);

                var assigned = false;
                for (var i = 0; i < model.Machines; i++) {
                    var scheduledJob = schedule[k];
                    if (L[i] <= scheduledJob.StartTime) {
                        L[i] = schedule[k].FinishTime;
                        schedule[k].AssignMachine(i);
                        assigned = true;
                        break;
                    }
                }
                if (!assigned)
                    throw new InvalidProgramException("Job could not be assigned to a machine!");
            }
            return schedule;
        }

        private List<(int j, double cAlpha)> CalculateAlphaJobCompletion(int jobcount, double[] cJ, double alpha) {
            double cAlpha(int j) => cJ[j] - ((1 - alpha) * model.Jobs[j].ProcessingTime);
            var cAlphaJ = new List<(int j, double cAlpha)>();

            for (var j = 0; j < jobcount; j++)
                cAlphaJ.Add((j, cAlpha(j)));

            return cAlphaJ;
        }

        private double[] CalculateRelaxedJobCompletion(double[,] x, int maxTime) {
            var c = new double[model.Jobs.Length];

            // Calculate relaxed finish time
            for (var j = 0; j < model.Jobs.Length; j++)
                for (var t = 0; t < maxTime; t++)
                    c[j] += (t + 1) * x[j, t];

            return c;
        }
    }
}
