﻿using System;
using System.Diagnostics;
using System.IO;

namespace SchedulingAlgorithms.Algorithms {
    public class LpSolve : ISolver {
        public LpSolve(string lpFile, int lpsolvetimeout = 10) {
            if (!File.Exists(lpFile))
                throw new FileNotFoundException($"LP File '{Path.GetFileName(lpFile)}' not found!");
            ModelFile = lpFile;
            LpSolveTimeout = lpsolvetimeout;
        }
        public string GetSolverExecutable() => "lp_solve.exe";
        public string ModelFile { get; }
        public int LpSolveTimeout { get; }
        public IOutputParser Solve(out TimeSpan solveTime) {
            var info = new ProcessStartInfo(GetSolverExecutable(), ModelFile) {
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                WorkingDirectory = Environment.CurrentDirectory
            };
            var process = Process.Start(info);
            while (true) {
                var output = process.StandardOutput.ReadToEndAsync();
                var error = process.StandardError.ReadToEndAsync();
                if (process.WaitForExit(LpSolveTimeout * 1000)) {
                    Console.WriteLine("lp_solve exited with code {0}!", process.ExitCode);
                    if (process.ExitCode == 0) {
                        solveTime = process.TotalProcessorTime;
                        return new LpSolveOutputParser(output.Result);
                    } else
                        throw new InvalidOperationException(error.Result);
                } else {
                    Console.WriteLine("Process has exited? {0}", process.HasExited);
                    Console.WriteLine("{0}s timeout reached for lp_solve, continue? (y/n)", LpSolveTimeout);
                    if (!XConsole.ReadBool())
                        throw new OperationCanceledException("Timeout reached and operation manually cancelled!");
                }
            }
        }
    }
}
