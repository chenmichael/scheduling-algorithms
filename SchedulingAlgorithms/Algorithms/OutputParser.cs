﻿namespace SchedulingAlgorithms.Algorithms {
    public interface IOutputParser {
        double ObjectiveValue();
        bool SolveSuccess();
        double GetDoubleValue(string variable);
        int GetIntValue(string variable);
    }
}