﻿#nullable enable


namespace SchedulingAlgorithms.Model {
    public class ScheduledJob : Job {
        private int? machineIndex;
        private double? startTime;
        private double? finishTime;
        public ScheduledJob(Job job, double? startTime = null, double? finishTime = null) : base(job.ProcessingTime, job.Weight) {
            this.startTime = startTime;
            this.finishTime = finishTime;
        }
#pragma warning disable CS8629 // Ein Werttyp, der NULL zulässt, kann NULL sein.
        public int MachineIndex { get => machineIndex.Value; }
        public double StartTime { get => startTime.Value; }
        public double FinishTime { get => finishTime.Value; }
#pragma warning restore CS8629 // Ein Werttyp, der NULL zulässt, kann NULL sein.
        public bool IsScheduled { get => startTime.HasValue; }
        public void RemoveSchedule() {
            startTime = null;
            finishTime = null;
        }
        public void ScheduleFromStart(double start) {
            startTime = start;
            finishTime = start + ProcessingTime;
        }
        public void ScheduleFromFinish(double finish) {
            startTime = finish - ProcessingTime;
            finishTime = finish;
        }
        public void AssignMachine(int machine) {
            machineIndex = machine;
        }

        public override string ToString()
             => $"Job {Name}: {base.ToString()}";
    }
}
