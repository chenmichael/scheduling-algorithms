﻿using SchedulingAlgorithms.Algorithms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
#nullable enable

namespace SchedulingAlgorithms.Model {
    public class RelaxedLP : LpModel {
        public RelaxedLP(ImportConfig cnfg) : base(cnfg) {
            UpperTimeLimit = UpperTimeBound();
            jobNumFormat = "D" + Jobs.Length.ToString().Length;
            timeNumFormat = "D" + UpperTimeLimit.ToString().Length;
        }
        private int UpperTimeBound() {
            var T = 1;
            foreach (var job in Jobs)
                T += job.ProcessingTime;
            foreach (var prec in Precedences)
                T += prec.Delay;
            return T;
        }
        internal void ParseResults(IOutputParser result) {
            //var maxfinish = 0;

            var xMatrix = new double?[Jobs.Length, UpperTimeLimit];

            for (var j = 0; j < Jobs.Length; j++) {
                //var job = Jobs[j];
                for (var t = 1; t <= UpperTimeLimit; t++) {
                    var varName = VarName(j, t);
                    var varValue = result.GetDoubleValue(varName);

                    xMatrix[j, t - 1] = varValue;
                    //if (varValue == 0) {
                    //    // Job not finished in this timeslot (can finish earlier or later)
                    //} else if (varValue == 1) {
                    //    // Job finished in this timeslot
                    //    if (job.IsScheduled)
                    //        // In LP Relaxation => Do not crash, full result matrix needed
                    //        throw new ArgumentException($"Job '{job.Name}' already scheduled to finish at time {job.FinishTime} (now {t})!");
                    //    else
                    //        job.ScheduleFromFinish(t);

                    //    maxfinish = Math.Max(maxfinish, job.FinishTime);
                    //} else
                    //    // In LP Relaxation => Job maybe finished
                    //    throw new ArgumentException($"Unkown result for job '{job.Name}' at time {t}!");
                }
            }

            //foreach (var job in Jobs) {
            //    if (!job.IsScheduled)
            //        throw new ArgumentException($"Unkown schedule time for job '{job.Name}' after lp_solve parse!");
            //    Console.WriteLine("Job '{0}' starts at {1} and is finished at {2} (exclusive)!", job.Name, job.StartTime, job.FinishTime);
            //}

            // Finish time is exclusive (no job executing at last finish time)
            Solution = new double[Jobs.Length, UpperTimeLimit];
            for (var j = 0; j < Jobs.Length; j++)
                for (var t = 0; t < UpperTimeLimit; t++)
#pragma warning disable CS8629 // Ein Werttyp, der NULL zulässt, kann NULL sein.
                    Solution[j, t] = xMatrix[j, t].Value;
#pragma warning restore CS8629 // Ein Werttyp, der NULL zulässt, kann NULL sein.
        }
        #region LP Program
        public void WriteLpProgram(StreamWriter sw) {
            WriteTargetFunction(sw);
            WriteConstraints(sw);
            // // No declarations needed as variables default to [0, +inf]
            // WriteDeclarations(sw);
        }
        //private void WriteDeclarations(StreamWriter sw) {
        //    sw.WriteLine("\n/* declarations */");
        //    for (var j = 0; j < Jobs.Length; j++) {
        //        var pj = Jobs[j].ProcessingTime;
        //        if (pj > UpperTimeLimit)
        //            break;
        //        sw.Write("bin");
        //        for (var t = pj + 1; t <= UpperTimeLimit; t++)
        //            sw.Write(" {0}", VarName(j, t));
        //        sw.WriteLine(";");
        //    }
        //}
        private void WriteConstraints(StreamWriter sw) {
            // constraint (1) by target function construction

            // constraint (2)
            WriteConstraint2(sw);

            // constraint (3)
            WriteConstraint3(sw);

            // constraint (4)
            WriteConstraint4(sw);

            // constraint (5)
            WriteConstraint5(sw);

            // constraint (6) by declaration
        }
        private void WriteConstraint5(StreamWriter sw) {
            sw.WriteLine("\n/* constraint (5) */");
            for (var j = 0; j < Jobs.Length; j++) {
                sw.WriteLine("/* job {0} can't yet be finished! */", j);
                var pj = Jobs[j].ProcessingTime;
                for (var t = 1; t <= pj; t++)
                    sw.WriteLine("{0} = 0;", VarName(j, t));
            }
        }
        private void WriteConstraint4(StreamWriter sw) {
            sw.WriteLine("\n/* constraint (4) */");
            foreach (var prec in Precedences) {
                var s_upper = UpperTimeLimit - prec.Delay - Jobs[prec.J].ProcessingTime;
                for (var s = 0; s <= s_upper; s++) {
                    sw.Write("delay_{0}_{1}_{2}:", prec.J.ToString(jobNumFormat), prec.K.ToString(jobNumFormat), s.ToString(timeNumFormat));
                    var t_upper = s + prec.Delay + Jobs[prec.K].ProcessingTime;
                    if (t_upper < 1)
                        sw.Write(" 0");
                    else
                        for (var t = 1; t <= t_upper; t++)
                            sw.Write(" {0}", VarName(prec.K, t));
                    sw.Write(" <=");
                    if (s < 1)
                        sw.Write(" 0");
                    else
                        for (var t = 1; t <= s; t++)
                            sw.Write(" {0}", VarName(prec.J, t));
                    sw.WriteLine(";");
                }
            }
        }
        private void WriteConstraint3(StreamWriter sw) {
            sw.WriteLine("\n/* constraint (3) */");
            for (var s = 1; s <= UpperTimeLimit; s++) {
                sw.Write("machines_working_{0}:", s.ToString(timeNumFormat));
                for (var j = 0; j < Jobs.Length; j++) {
                    var upper = Math.Min(s + Jobs[j].ProcessingTime - 1, UpperTimeLimit);
                    for (var t = s; t <= upper; t++)
                        sw.Write(" {0}", VarName(j, t));
                }
                sw.WriteLine(" <= {0};", Machines);
            }
        }
        private void WriteConstraint2(StreamWriter sw) {
            sw.WriteLine(";\n\n/* constraint (2) (every job will complete at one single time) */");
            for (var j = 0; j < Jobs.Length; j++) {
                sw.Write("job_{0}_complete:", j);
                for (var t = 1; t <= UpperTimeLimit; t++)
                    sw.Write(" {0}", VarName(j, t));
                sw.WriteLine(" = 1;");
            }
        }
        private void WriteTargetFunction(StreamWriter sw) {
            sw.WriteLine("/* target function and constraint (1) */");
            sw.Write("min:");
            for (var j = 0; j < Jobs.Length; j++)
                for (var t = 1; t <= UpperTimeLimit; t++)
                    sw.Write(" {0} {1}", (Jobs[j].Weight * t).ToString(CultureInfo.InvariantCulture), VarName(j, t));
        }
        #endregion
        private string VarName(int jobNumber, int timeStamp) => $"x_{jobNumber.ToString(jobNumFormat)}_{timeStamp.ToString(timeNumFormat)}";
        public readonly int UpperTimeLimit;
        private readonly string jobNumFormat;
        private readonly string timeNumFormat;
    }
}
