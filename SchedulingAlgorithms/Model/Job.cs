﻿using Newtonsoft.Json;
#nullable enable

namespace SchedulingAlgorithms.Model {
    public class Job {
        public Job() { }
        public Job(int time, double weight = 0.0d, string? name = null) : this() {
            Weight = weight;
            ProcessingTime = time;
            Name = name;
        }
        public Job(Job job, string? name = null) : this(job.ProcessingTime, job.Weight, name) { }
        [JsonProperty(PropertyName = "weight")]
        public double Weight { get; set; }
        [JsonProperty(PropertyName = "time")]
        public int ProcessingTime { get; set; }
        [JsonIgnore]
        public string? Name { get; }
        public override string ToString()
             => $"w = {Weight}, p = {ProcessingTime}";
    }
}
