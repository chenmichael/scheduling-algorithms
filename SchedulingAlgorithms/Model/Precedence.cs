﻿using Newtonsoft.Json;
#nullable enable

namespace SchedulingAlgorithms.Model {
    public class Precedence<T> {
        public Precedence(T j, T k, int delay) {
            J = j;
            K = k;
            Delay = delay;
        }
        [JsonProperty(PropertyName = "j")]
        public T J { get; set; }
        [JsonProperty(PropertyName = "k")]
        public T K { get; set; }
        [JsonProperty(PropertyName = "delay")]
        public int Delay { get; set; }
    }
}
