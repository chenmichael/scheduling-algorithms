﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
#nullable enable

namespace SchedulingAlgorithms.Model {
    public class ImportConfig {
        [JsonConstructor]
        public ImportConfig() {
            Jobs = new Dictionary<string, Job>();
            Precedences = new List<Precedence<string>>();
            Name = string.Empty;
        }
        public ImportConfig(int machineCount, string configName, Dictionary<string, Job> jobs, List<Precedence<string>> precedences) {
            Machines = machineCount;
            Name = configName;
            Jobs = jobs;
            Precedences = precedences;
        }
        public static ImportConfig CreateNewConfig() {
            // get number of jobs
            Console.Write("How many jobs: ");
            var jobCount = XConsole.ReadInt();

            // get number of machines
            Console.Write("How many machines: ");
            var m = XConsole.ReadInt();

            // get jobs
            var jobs = XConsole.ReadJoblist(jobCount);

            // get precedences
            Console.WriteLine("Give job precedences (comma sep list of precedences 0<1,1<2,...):");
            var input = Console.ReadLine().Split(',', StringSplitOptions.RemoveEmptyEntries);
            var djk = new List<Precedence<string>>();
            foreach (var item in input) {
                var prec = item.Split('<', StringSplitOptions.RemoveEmptyEntries);
                if (prec.Length != 2) {
                    Console.WriteLine("Skipping invalid precedence: '{0}'!", item);
                    continue;
                }
                try {
                    var j = prec[0];
                    var k = prec[1];
                    if (!jobs.ContainsKey(j))
                        throw new FormatException($"Unknown job '{j}'!");
                    if (!jobs.ContainsKey(k))
                        throw new FormatException($"Unknown job '{k}'!");
                    Console.WriteLine("Precedence delay for {0}<{1}: ", j, k);
                    var delay = XConsole.ReadInt();
                    djk.Add(new Precedence<string>(j, k, delay));
                } catch (FormatException ex) {
                    Console.WriteLine("Skipping parse error: {0}", ex.Message);
                }
            }

            Console.WriteLine("Name config: ");
            var name = Console.ReadLine();

            return new ImportConfig(m, name, jobs, djk);
        }
        [JsonProperty(PropertyName = "jobs")]
        public Dictionary<string, Job> Jobs;
        [JsonProperty(PropertyName = "precedences")]
        public List<Precedence<string>> Precedences;
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "machines")]
        public int Machines { get; set; }
    }
}
