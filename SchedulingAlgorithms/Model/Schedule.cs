﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#nullable enable

namespace SchedulingAlgorithms.Model {
    public class Schedule {
        public Schedule(Job[] jobs) {
            // Copy job array and change type to Scheduled job
            this.jobs = new ScheduledJob[jobs.Length];
            for (var i = 0; i < jobs.Length; i++)
                this.jobs[i] = new ScheduledJob(jobs[i]);
        }
        private readonly ScheduledJob[] jobs;
        public ScheduledJob this[int i] {
            get => jobs[i];
            set => jobs[i] = value;
        }
        public int JobCount => jobs.Length;
        public void ScheduleJobStart(int j, int starttime) => jobs[j].ScheduleFromStart(starttime);
        public void ScheduleJobFinish(int j, int finishtime) => jobs[j].ScheduleFromFinish(finishtime);
    }
}
