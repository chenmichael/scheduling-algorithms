﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
#nullable enable

namespace SchedulingAlgorithms.Model {
    public static class ModelParser {
        private const string InteractiveBreakCommand = "break";
        private static readonly Regex headerRgx = new Regex(@"^\s*(?<key>\w+)\s*=\s*(?<value>\w+)\s*$");
        private static readonly Regex jobRgx = new Regex(@"^\s*(?<name>\w+)\s*\:\s*(?<time>\d+)(?:\-\s*(?<weight>\d+(?:\.\d+)?))?\s*$");
        private static readonly Regex precedenceRgx = new Regex(@"(?=\s*(?<j>\w+)\s*-(?<delay>\d+)?>\s*(?<k>\w+)\s*)");
        public static ImportConfig ParseInteractive() {
            InitParse(out var linenum, out var machineCount, out var configName, out var jobs, out var precedences);

            while (true) {
                var line = Console.ReadLine();
                linenum++;

                if (line == InteractiveBreakCommand)
                    try {
                        return FinalizeParse(machineCount, configName, jobs, precedences);
                    } catch (FormatException e) {
                        Console.WriteLine("Cannot break: {0}", e.Message);
                    }

                try {
                    if (ParseLine(ref linenum, ref machineCount, ref configName, ref jobs, ref precedences, line))
                        continue;

                    throw new FormatException($"Line {linenum}: Syntax error! Cannot parse '{line}'!");
                } catch (FormatException e) {
                    Console.WriteLine(e.Message);
                }
            }
        }
        public static ImportConfig Parse(string file) {
            using var sw = new StreamReader(file);
            InitParse(out var linenum, out var machineCount, out var configName, out var jobs, out var precedences);

            while (sw.ReadLine() is string line) {
                linenum++;

                if (ParseLine(ref linenum, ref machineCount, ref configName, ref jobs, ref precedences, line))
                    continue;

                throw new FormatException($"Line {linenum}: Syntax error! Cannot parse '{line}'!");
            }

            return FinalizeParse(machineCount, configName, jobs, precedences);
        }

        private static ImportConfig FinalizeParse(int? machineCount, string? configName, Dictionary<string, Job> jobs, List<Precedence<string>> precedences) {
            if (!machineCount.HasValue)
                throw new FormatException($"Syntax error! Missing property 'n' for machine count!");
            if (configName is null)
                throw new FormatException($"Syntax error! Missing property 'name' for config name!");
            return new ImportConfig(machineCount.Value, configName, jobs, precedences);
        }

        private static void InitParse(out int linenum, out int? machineCount, out string? configName, out Dictionary<string, Job> jobs, out List<Precedence<string>> precedences) {
            linenum = 0;
            machineCount = null;
            configName = null;
            jobs = new Dictionary<string, Job>();
            precedences = new List<Precedence<string>>();
        }

        private static bool ParseLine(ref int linenum, ref int? machineCount, ref string? configName, ref Dictionary<string, Job> jobs, ref List<Precedence<string>> precedences, string line) {
            if (ParseEmptyLines(line))
                return true;
            if (ParseComments(line))
                return true;
            if (ParseHeader(linenum, line, ref machineCount, ref configName))
                return true;
            if (ParseJob(linenum, line, ref jobs))
                return true;
            if (ParsePrecedences(linenum, line, in jobs, ref precedences))
                return true;

            return false;
        }
        private static bool ParseEmptyLines(string line) => string.IsNullOrWhiteSpace(line);
        private static bool ParseComments(string line) => line.TrimStart().StartsWith('#');
        private static bool ParsePrecedences(int linenum, string line, in Dictionary<string, Job> jobs, ref List<Precedence<string>> precedences) {
            var precedenceMatches = precedenceRgx.Matches(line);
            if (precedenceMatches.Count != 0) {
                for (var i = 0; i < precedenceMatches.Count; i++) {
                    var precedenceMatch = precedenceMatches[i];
                    if (precedenceMatch.Success) {
                        var jGroup = precedenceMatch.Groups["j"];
                        var j = jGroup.Value;
                        var kGroup = precedenceMatch.Groups["k"];
                        var k = kGroup.Value;
                        var delay = 0;
                        if (!jobs.ContainsKey(j))
                            throw new FormatException($"Line {linenum}, Column {jGroup.Index}: Unkown job '{j}', declare job before its precedence properties!");
                        if (!jobs.ContainsKey(k))
                            throw new FormatException($"Line {linenum}, Column {kGroup.Index}: Unkown job '{k}', declare job before its precedence properties!");
                        var delayGroup = precedenceMatch.Groups["delay"];
                        if (delayGroup.Success) {
                            var precdelayString = delayGroup.Value;
                            if (!int.TryParse(precdelayString, out delay))
                                throw new FormatException($"Line {linenum}, Column {delayGroup.Index}: Invalid precedence delay '{precdelayString}' for precedence {j}->{k}!");
                            if (delay < 0)
                                throw new FormatException($"Line {linenum}, Column {delayGroup.Index}: Error: negative precedence delay for precedence {j}->{k}!");
                        }
                        var exists = precedences.FirstOrDefault(i => i.J == j && i.K == k);
                        if (exists is Precedence<string> previous)
                            if (previous.Delay == delay)
                                Console.WriteLine($"Warning: Line {linenum}, Column {precedenceMatch.Index}: Precedence {j} < {k} was previously defined with the same delay!");
                            else
                                throw new FormatException($"Line {linenum}, Column {precedenceMatch.Index}: Precedence {j} < {k} was previously defined with another delay!");
                        precedences.Add(new Precedence<string>(j, k, delay));
                        Console.WriteLine("Added precedence on {0} < {1} with delay {2}!", j, k, delay);
                    } else
                        throw new FormatException($"Line {linenum}: Syntax error! Unsuccessful match in match collection!");
                }
                return true;
            }
            return false;
        }
        private static bool ParseHeader(int linenum, string line, ref int? machineCount, ref string? configName) {
            var headerMatch = headerRgx.Match(line);
            if (!headerMatch.Success)
                return false;
            var keyGroup = headerMatch.Groups["key"];
            var key = keyGroup.Value;
            var valueGroup = headerMatch.Groups["value"];
            var value = valueGroup.Value;
            switch (key) {
            case "n":
                if (machineCount.HasValue)
                    throw new FormatException($"Line {linenum}, Column {keyGroup.Index}: Duplicate machine count key '{key}'!");
                if (int.TryParse(value, out var count)) {
                    if (machineCount < 1)
                        throw new FormatException($"Line {linenum}, Column {valueGroup.Index}: Non natural machine count '{value}'!");
                    machineCount = count;
                    Console.WriteLine("Set machine count to {0}!", count);
                } else
                    throw new FormatException($"Line {linenum}, Column {valueGroup.Index}: Invalid machine count '{value}'!");
                break;
            case "name":
                if (!(configName is null))
                    throw new FormatException($"Line {linenum}, Column {keyGroup.Index}: Duplicate config name key '{key}'!");
                configName = value;
                Console.WriteLine("Set config name to {0}!", value);
                break;
            default:
                throw new FormatException($"Line {linenum}, Column {keyGroup.Index}: Unknown property key: {key}!");
            }
            return true;
        }
        private static bool ParseJob(int linenum, string line, ref Dictionary<string, Job> jobs) {
            var jobMatch = jobRgx.Match(line);
            if (!jobMatch.Success)
                return false;
            var nameGroup = jobMatch.Groups["name"];
            var jobname = nameGroup.Value;
            if (jobs.ContainsKey(jobname))
                throw new FormatException($"Line {linenum}, Column {nameGroup.Index}: Job '{jobname}' was previously declared!");
            var timeGroup = jobMatch.Groups["time"];
            var jobtimeString = timeGroup.Value;
            var jobweight = 1.0d;
            if (!int.TryParse(jobtimeString, out var jobtime))
                throw new FormatException($"Line {linenum}, Column {timeGroup.Index}: Invalid job time '{jobtimeString}' for job {jobname}!");
            var weightGroup = jobMatch.Groups["weight"];
            if (weightGroup.Success) {
                var jobweightString = weightGroup.Value;
                if (!double.TryParse(jobweightString, NumberStyles.Float, CultureInfo.InvariantCulture, out jobweight))
                    throw new FormatException($"Line {linenum}, Column {weightGroup.Index}: Invalid job weight '{jobweightString}' for job {jobname}!");
                if (jobweight < 0.0d)
                    throw new FormatException($"Line {linenum}, Column {weightGroup.Index}: Error: negative weight for job {jobname}!");
            }
            jobs.Add(jobname, new Job(jobtime, jobweight));
            Console.WriteLine("Added job '{0}' with processing time {1} and weight {2}!", jobname, jobtime, jobweight);
            return true;
        }
    }
}
