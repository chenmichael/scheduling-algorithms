﻿using System;
using System.Collections.Generic;
using System.Linq;
#nullable enable

namespace SchedulingAlgorithms.Model {
    public class LpModel {
        public LpModel(ImportConfig cnfg) {
            Name = cnfg.Name;
            Machines = cnfg.Machines;
            Jobs = cnfg.Jobs.Select(i => new Job(i.Value, i.Key)).ToArray();
            jobMapping = GetMapping(Jobs);
            Precedences = cnfg.Precedences.Select(i => new Precedence<int>(jobMapping[i.J], jobMapping[i.K], i.Delay)).ToArray();
        }
        private static Dictionary<string, int> GetMapping(Job[] jobs) {
            var dict = new Dictionary<string, int>();
            for (var i = 0; i < jobs.Length; i++) {
                var job = jobs[i];
                if (job.Name is null)
                    throw new ArgumentNullException($"Job at index {i} has no name!");
                else
                    dict.Add(job.Name, i);
            }
            return dict;
        }
        public string Name { get; }
        public int Machines { get; }
        public bool IsSolved { get => !(Solution is null); }

        public readonly Job[] Jobs;
        public readonly Precedence<int>[] Precedences;
        public double[,]? Solution;
        private readonly Dictionary<string, int> jobMapping;
    }
}