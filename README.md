# Scheduling Algorithms

This project was developed by @chenmichael as part of the seminar **Advances in Scheduling Algorithms**. It features an implementations the approximation method published in **Approximating total weighted completion time on identical parallel machines with precedence constraints and release dates** [Jäger, 2018] (see [here](https://www.sciencedirect.com/science/article/abs/pii/S0167637718301299)).

## Problem

There is a set $`N=\left\{1,\ldots,n\right\}`$ of jobs and for $`\forall j \in N`$ there is a weight $`w_j \geq 0`$ and a processing time $`p_j \geq 0`$ associated with the job. There are $`m`$ identical machines, each able to process one job at a time. The jobs are processed nonpreemtively on one of the $`m`$ machines. Additionally there exists a strict partially ordered set $`\prec`$ on $`N`$ to specify precedence constraints: $`\forall j \prec k`$ there is a precedence delay $`d_{j,k} \geq 0`$ meaning that job $`k`$ can only start $`d_{j,k}`$ time units after $`j`$ has finished. A release date for job $`j`$ can be modelled by adding a dummy job $`e`$ of length $`0`$ with $`d_{e,j} = r_j`$ (precedence delay of $`e`$ is release date of $`j`$) that preceeds $`j`$: $`e \prec j`$.

## Solution

### Prerequisites

This program requires [LP Solve](https://sourceforge.net/projects/lpsolve/) to be installed and added to the path environment variable. Alternatively you can extract the folder to the working directory of this executable.

### Building the program

Build the program using [`dotnet`](https://docs.microsoft.com/de-de/dotnet/core/tools/dotnet?tabs=netcore21):

```
dotnet build --configuration Release
```

Do check that the pipeline has passed for the specific commit you are building!

[![pipeline status](https://gitlab.com/chenmichael/scheduling-algorithms/badges/master/pipeline.svg)](https://gitlab.com/chenmichael/scheduling-algorithms/commits/master)

### Starting the program

This program can be started in interactive mode with `-i` (default):

```
SchedulingAlgorithm.exe -i
```

Or with a [model file](#model-files) as an argument:

```
SchedulingAlgorithm.exe modelfile.mdl
```

### Model files

The following Job flow chart being processed on two machines:

```mermaid
graph LR
	A((A<br/>2))
	B((B<br/>5))
	C((C<br/>3))
	D((D<br/>6))
	E((E<br/>3))
	F((F<br/>2))
	G((G<br/>1))
	H((H<br/>2))
	A -- 3 --> B
	B --> D
	D --> H
	A -- 5 -->C
	C --> E
	E --> G
	G --> H
	B --> F
	F --> G
```

can be modelled with the following model file:

```
# Model headers
name=management
n=2
# Declarations
A:2
B:5
C:3
D:6
E:3
F:2
G:1
H:2
# Precedence constraints 
A-3>B->D->H
A-5>C->E->G->H
B->F->G
```

#### Syntax

Lines starting with a `#` sign will be ignored by the parser (can be used for comments). The general structure of a model file is:

1. Headers
2. Job declarations
3. Precedence constraints

While it is not necessary to keep this order it is still recommended as jobs must always be declared before being used in a precedence constraint.

#### Headers

Headers have the format:

```
key=value
```

Neither `key` nor `value` may contain whitespaces. They may only contain alphanumerical chars or `_` (underscores).

The headers `name` and `n` must be specified where `name` is the name of the model and will be used for naming temporary or result files and `n` is the number of machines that are available for processing jobs.

#### Job declarations

Job declarations have the format:

```
name:time
name:time-weight
```

- `name` is the name of the job and may only contain alphanumerical chars or `_` (underscores), when referred to the jobname in precedences it is always **case sensitive** so `a` and `A` are two different jobs,
- `time` is the processing time $`p_j`$ of the job and it must be an unsigned integer value **without** preceeding sign(s) `+`,
- `weight` is the weight of the job $`w_j`$, an unsigned double precision value with '.' as decimal point without preceeding sign(s) `+`. If omitted it will default to 1.

#### Precedence constraints

Precedence constraints have the format:

```
A->B
A-delay>B
```

- `A` and `B` are **case sensititve** names of different jobs that were **previously** declared,
- `delay` is an optional argument specifying the delay between executions of job `A` and `B`, defined as $`d_{A,B} : A \prec B`$,
- Precedences can be specified multiple times for the same job pair as long as their delay is the same (will yield a warning), otherwise an error will occur (expected behaviour / not overwriting previous specs).

Precedence constraints can be chained:

```
A->B-4>C->D->E
A-0>B-10>D->E
```

Note that `A->B` and `D->E` each are specified multiple times. This will work as expected but will raise two warnings. If one would additionally specify `A-1>B` a parse error will be thrown.
